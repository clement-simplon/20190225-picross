# Architecture du projet.
1. Créer 2 branches:
    * 1 pour dire si la case est présumée vide ou pleine grâce au clic gauche ou droit (branche "clic-left-right".)
    * 1 pour vérifier si la grille complétée est juste (branche "check-picross".)

2. Créer un dossier html pour l'interface web du jeu.

3. Créer un dossier js pour les fonctionnalités du jeu.
    * 1 fichier.js pour les clics.
    * 1 fichier.js pour la validation.

# Arbre des fonctions
1. Créer un arbre des fonctions à utiliser en Javascript.
    * 1 fonction pour initialiser la grille (créer la grille + indices chiffrés.)
    * 1 fonction pour l'afficher.
    * 1 fonction "clic gauche" (vide.)
    * 1 fonction "clic droit" (plein.)
    * 1 fonction "annuler" (retour à l'état initial.)
    * 1 fonction pour la validation.
    * 1 fonction qui calcule la grille solution.
    * 1 fonction "gagné".
    * 1 fonction "perdu".
    * 1 fonction "réinitialisation de partie".

# Façon de pocéder.
1. Créer un fichier.html ("picross.html")

2. Créer un header et un footer bleus (à voir!).
    * Dans le header: un titre h1 ("Picross game")
    * Un titre h2 ("Simplon Chambéry #Promo1")
    * un titre h2 ("noms, prénoms") des créateurs du jeu.

3. Le corps de la page doit être blanc.
    * Encadré bleu (vraiment???) avec la grille au centre de la page.
    * Clic gauche: on suppose que la case est vide.
    * Clic droit: on suppose que la case est pleine.

# Questions
1. Comment créer l'encadré avec la grille au centre de la page?

2. Comment réaliser le bandeau à droite de la grille? (Timer + biscuit + pause + quitter.)

3. Copier/coller du code du fichier .json dans le fichier .js? (pour faire la grille.)

4. Si je re-clique sur la case, est-ce que je reviens en arrière? (décoche?)

5. Système de vérification de la grille? -> Comparaison de la grille du joueur à la grille réponse? Calcul automatique de la solution?

# Qui fait quoi?
1. Clément.
    * Chercher comment initialiser la grille en js.

2. Pascaline.
    * Création de l'interface web (fichier picross.html.)