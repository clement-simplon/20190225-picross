// Constantes

const SQUARE_WIDTH_TITANIC = 90; //Largeur d'un carré pour le nonogram titanic
const SQUARE_WIDTH_CHAMPIGNON = 42; //Largeur d'un carré pour le nonogram champignon
const SQUARE_WIDTH_CERF = 20; //Largeur d'un carré pour le nonogram octopus
const NB_MAX_CLUES_TITANIC = 1; // Nombres max d'indices pour le nonogram titanic
const NB_MAX_CLUES_CHAMPIGNON = 4; // Nombres max d'indices pour le nonogram champignon
const NB_MAX_CLUES_CERF = 6; // Nombres max d'indices pour le nonogram octopus
const SQUARE_MARGIN = 1; // Espace entre les carrés
const COLOR_OF_SQUARE_CLUES = "#8463c2";
const COLOR_OF_SQUARE_EMPTY = "#e3e3e3";
const COLOR_OF_SQUARE_CLEAR = "#ffffff"
const COLOR_OF_SQUARE_FULL = "#131313";

// Variables

let nbMaxClues; // Nombre max d'indices
let squareWidth; // Largeur d'un carré
let gameWidth; // Largeur du jeu 
let gameHeight; // Hauteur du jeu
let gridArray; // Grille de jeu
let win = false; 
let timerID;
let startTime = 0;
let picrossColumns;
let picrossRows;
let titanicColumns = [
    [0],
    [1],
    [5],
    [4],
    [0]
];
let titanicRows = [
    [1],
    [2],
    [2],
    [3],
    [2]
];
let champignonColumns = [
    [1],
    [2, 1],
    [3, 1],
    [5, 1],
    [2, 7],
    [6, 1],
    [1, 1, 2, 2],
    [10],
    [4, 1],
    [3, 3]
];
let champignonRows = [
    [6],
    [4, 3],
    [3, 5],
    [5, 3],
    [5],
    [4],
    [1, 1],
    [1, 1, 1],
    [1, 2, 4],
    [1, 1, 4, 1]
];
let cerfColumns = [
    [1],
    [2],
    [1, 3],
    [2, 3],
    [2, 2, 3],
    [7, 3],
    [4, 4, 4],
    [3, 2, 1, 4],
    [5, 4],
    [3, 5],
    [3, 5],
    [3, 5],
    [2, 2, 5],
    [3, 2, 1, 5],
    [4, 7, 2, 4],
    [10, 2, 5],
    [1, 1, 2, 6, 9],
    [4, 1, 1, 4, 12],
    [5, 17],
    [5, 2, 16],
    [21],
    [22],
    [24],
    [7, 3, 3, 4, 4],
    [4, 2, 1, 2, 3],
    [1, 3, 7],
    [5, 6],
    [7, 4],
    [2, 2],
    [1]
];
let cerfRows = [
    [1],
    [1],
    [1, 3, 1],
    [1, 2, 1, 2, 1],
    [4, 1, 1, 1, 2],
    [2, 2, 3, 2],
    [2, 2, 1, 2, 2, 4],
    [3, 2, 1, 1, 3, 1],
    [3, 5, 1, 3, 1],
    [2, 1, 4, 4, 1, 2],
    [4, 2, 5, 2],
    [4, 2, 5, 5],
    [4, 3, 10],
    [12, 3, 3],
    [14],
    [9],
    [12],
    [4, 5, 1],
    [7, 2],
    [10],
    [11],
    [13],
    [7, 4],
    [7, 2],
    [7],
    [5, 8],
    [18],
    [22],
    [23],
    [24]
];


/////////////////////////////FONCTIONS///////////////////////////////////////////////////////


// Fonction qui empeche au clic droit d'afficher le menu contextuel
function preventDefaultClickRight() {
    document.addEventListener('contextmenu', event => event.preventDefault());
}

// 
function refreshGame(ctx) {
    ctx.clearRect(0, 0, gameWidth, gameHeight);
}


// Fonction qui retourne l'élément avec id='canvasElem'
function getCanva() {
    return document.getElementById('canvasElem');
}


// Fonction qui retourne l'élément context en 2d
function getContext() {
    // Recupère l'élément canva dans la variable elem
    let elem = getCanva();

    // Si il n'y a pa de canvas ou de context 2d, sortir de la fonction
    if (!elem || !elem.getContext) {
        return;
    }

    // Retourne un contexte de dessin en 2d
    return elem.getContext('2d');
}

// Fonction qui initialise la taille de la zone de jeu
function gameInit() {
    // Recupère l'élément canva dans la variable elem
    let elem = getCanva();

    // Si il n'y a pa de canvas ou de context 2d, sortir de la fonction
    if (!elem || !elem.getContext) {
        return;
    }
    // Conserve les dimensions du canvas dans deux variables
    gameWidth = elem.width;
    gameHeight = elem.height;
}


// Fonction qui initialise la grille de jeu
function initGridArray() {

    // crée un nouveau tableau (à une dimension) de taille maximum =
    // aux nbs de colonnes du tableau titanic
    gridArray = new Array(picrossRows.length);

    for (let i = 0; i < picrossRows.length; i++) {

        // crée un nouveau tableau (à deux dimension) de taille maximum =
        // aux nbs de lignes du tableau titanic
        gridArray[i] = new Array(picrossColumns.length)

        for (let j = 0; j < picrossColumns.length; j++) {

            //initialise les valeurs à 0
            gridArray[i][j] = 0;
        }
    }
}

// Fonction qui affiche la grille de jeu
function displayGrid(ctx) {

    /****************affiche les cases indices du haut****************************/
    for (let i = 0; i < picrossColumns.length; i++) {

        //
        if (nbMaxClues > 1) {
            for (let j = 0; j < picrossColumns[i].length; j++) {

                // Règle la couleur du dessin que l'on va faire (pour les cases indices)
                ctx.fillStyle = COLOR_OF_SQUARE_CLUES;

                // Dessine un rectangle (x,y,largeur,hauteur)
                ctx.fillRect(
                    i * (squareWidth + SQUARE_MARGIN),
                    (j + picrossRows.length) * (squareWidth + SQUARE_MARGIN),
                    squareWidth,
                    squareWidth);

                // Dessine du texte (texte,x,y) (écrit les indices dans les bandeaux bleus)
                ctx.strokeText(
                    picrossColumns[i][j],
                    i * (squareWidth + SQUARE_MARGIN) + (squareWidth / 2),
                    (j + picrossRows.length) * (squareWidth + SQUARE_MARGIN) + (squareWidth / 2))
            }
            //
        } else {

            // Règle la couleur du dessin que l'on va faire (pour les cases indices)
            ctx.fillStyle = COLOR_OF_SQUARE_CLUES;

            // Dessine un rectangle (x,y,largeur,hauteur)
            ctx.fillRect(
                i * (squareWidth + SQUARE_MARGIN),
                picrossRows.length * (squareWidth + SQUARE_MARGIN),
                squareWidth,
                squareWidth);

            // Dessine du texte (texte,x,y) (écrit les indices dans les bandeaux bleus)
            ctx.strokeText(
                picrossColumns[i],
                i * (squareWidth + SQUARE_MARGIN) + (squareWidth / 2),
                picrossRows.length * (squareWidth + SQUARE_MARGIN) + (squareWidth / 2))
        }


    }
    /****************affiche les cases indices de gauche****************************/
    for (let i = 0; i < picrossRows.length; i++) {

        //
        if (nbMaxClues > 1) {
            for (let j = 0; j < picrossRows[i].length; j++) {
                // Règle la couleur du dessin que l'on va faire (pour les cases indices)
                ctx.fillStyle = COLOR_OF_SQUARE_CLUES;

                // Dessine un rectangle (x,y,largeur,hauteur)
                ctx.fillRect(
                    (j + picrossColumns.length) * (squareWidth + SQUARE_MARGIN),
                    i * (squareWidth + SQUARE_MARGIN),
                    squareWidth,
                    squareWidth);

                // Dessine du texte (texte,x,y) (écrit les indices dans les bandeaux bleus)
                ctx.strokeText(
                    picrossRows[i][j],
                    (j + picrossColumns.length) * (squareWidth + SQUARE_MARGIN) + (squareWidth / 2),
                    i * (squareWidth + SQUARE_MARGIN) + (squareWidth / 2))
            }
            //
        } else {
            // Règle la couleur du dessin que l'on va faire (pour les cases indices)
            ctx.fillStyle = COLOR_OF_SQUARE_CLUES;

            // Dessine un rectangle (x,y,largeur,hauteur)
            ctx.fillRect(
                picrossColumns.length * (squareWidth + SQUARE_MARGIN),
                i * (squareWidth + SQUARE_MARGIN),
                squareWidth,
                squareWidth);

            // Dessine du texte (texte,x,y) (écrit les indices dans les bandeaux bleus)
            ctx.strokeText(
                picrossRows[i],
                picrossColumns.length * (squareWidth + SQUARE_MARGIN) + (squareWidth / 2),
                i * (squareWidth + SQUARE_MARGIN) + (squareWidth / 2))

        }
    }

    /***********************affiche les cases grises*********************************/

    for (let i = 0; i < gridArray.length; i++) {
        for (let j = 0; j < gridArray[i].length; j++) {

            // Règle la couleur du dessin que l'on va faire (pour les cases non remplies)
            ctx.fillStyle = COLOR_OF_SQUARE_EMPTY;

            // Dessine un rectangle (x,y,largeur,hauteur)
            ctx.fillRect(
                j * (squareWidth + SQUARE_MARGIN),
                i * (squareWidth + SQUARE_MARGIN),
                squareWidth,
                squareWidth);
        }
    }
}


// Fonction qui recherche dans quel case l'utilisateur a cliqué et renvoie la case du tableau correspondante
function findSquare(x, y) {
    for (let i = 0; i < (picrossColumns.length); i++) { // i démarre à 1 (décalage indices); tant que i < au nb de colonnes du tableau titanic + 1 (pour les cases indice); va dans la colonne d'à coté.
        for (let j = 0; j < (picrossRows.length); j++) { // j demarre à 1 (décalage indices); tant que j < au nb de lignes du tableau titanic +1 (pour les cases indice); va dans la ligne d'à côté.

            // Si les coordonnees x et y sont comprises dans la case actuelle (regarde où se situe le clic)
            if (
                i < (x / (squareWidth + SQUARE_MARGIN)) && // si les coordonnées de x sont comprises dans la largeur d'une case
                (x / (squareWidth + SQUARE_MARGIN)) < (i + 1) &&
                j < y / (squareWidth + SQUARE_MARGIN) && // et si j est compris dans la hauteur d'une case
                (y / (squareWidth + SQUARE_MARGIN)) < (j + 1)) {
                return [i, j];
            }

        }
    }
}




// Fonction qui change la couleur d'une case en fonction du type de clic et
// des coordonnées x et y + change la valeur d'une case dans le tableau gridArray
function changeColor(x, y, clic, ctx) {
    let coord = findSquare(x, y);

    // Si le clic est un clic gauche
    if (clic === 0) {

        // Règle la couleur du dessin que l'on va faire (pour les cases vides) 
        ctx.fillStyle = COLOR_OF_SQUARE_CLEAR;
        // Met la valeur 0 (case vide) dans le tableau de la grille de jeu
        gridArray[coord[0]][coord[1]] = 0; // Fais abstraction des indices; quand tu clic la valeur est 0 (= case vide)
    }

    // Sinon si le clic est un clic droit
    else if (clic === 2) {

        // Règle la couleur du dessin que l'on va faire (pour les cases pleines)
        ctx.fillStyle = COLOR_OF_SQUARE_FULL;
        // Met la valeur 1 (case pleine) dans le tableau de la grille de jeu
        gridArray[coord[0]][coord[1]] = 1; // Fais abstraction des indices; quand tu clic la valeur est 1 (= case pleine)
    }

    // Dessine un rectangle (x,y,largeur,hauteur)
    ctx.fillRect(
        coord[0] * (squareWidth + SQUARE_MARGIN),
        coord[1] * (squareWidth + SQUARE_MARGIN),
        squareWidth,
        squareWidth);
}

// Fonction qui compare deux arrays, renvoie faux si non égal - renvoie true si égal
function compareArray(tab1, tab2) {
    let ok = false;
    for (let i = 0; i < tab1.length; i++) {
        if (tab1[i] === tab2[i]) {
            ok = true;
        } else {
            return false;
        }
    }
    if (ok === true) {
        return true;
    }
}

// Fonction qui vérifie le résultat de la grille de jeu par rapport au indices 
function checkIfWin() {

    // vérifie les indices de gauche

    for (let i = 0; i < gridArray.length; i++) {

        let counterArray = new Array();
        let k = 0;

        for (let j = 0; j < gridArray[i].length; j++) {

            let previousVal = null;

            // Si j>0, on a une valeur précédente  
            if (j > 0) {
                previousVal = gridArray[i][j - 1];
            }

            if (gridArray[i][j] === 1 && previousVal === 1) {
                counterArray[k]++;

            } else if (gridArray[i][j] === 1 && previousVal != 1) {
                counterArray[k] = 1;

            } else if (gridArray[i][j] === 0 && previousVal === 1) {
                k++;
            }
        }

        if (counterArray[0] === undefined) {
            counterArray[0] = 0;
        }

        if (compareArray(counterArray, picrossColumns[i]) === false) {
            return false;
        }

    }

    // vérifie les indices du haut

    for (let i = 0; i < gridArray.length; i++) {

        let counterArray = new Array();
        let k = 0;

        for (let j = 0; j < gridArray[i].length; j++) {

            let previousVal = null;

            // Si j>0, on a une valeur précédente
            if (j > 0) {
                previousVal = gridArray[j - 1][i];
            }

            if (gridArray[j][i] === 1 && previousVal === 1) {
                counterArray[k]++;

            } else if (gridArray[j][i] === 1 && previousVal != 1) {
                counterArray[k] = 1;

            } else if (gridArray[j][i] === 0 && previousVal === 1) {
                k++;
            }

        }

        if (counterArray[0] === undefined) {
            counterArray[0] = 0;
        }

        if (compareArray(counterArray, picrossRows[i]) === false) {
            return false;
        }
    }

    // Si on a eu juste à chaque vérifications alors on retourne true
    return true;
}




// Fonction qui affiche un message si on a gagné ou pas
function displayWin() {
    let win = checkIfWin();
    //
    if (win) {
        alert("C'est gagné !!");
        timerStop();
    }
    //
    else {
        alert("Ce n'est pas encore ça.")
    }

}

// variable d'évènement qui obtient les coordonnées x et y (dans le canvas)
// lors d'un clic de souris
let mouseEvent = function (event) {

    //Récupère le contexte de dessin 2D canvas
    context = getContext();
    if (!context) {
        return;
    }
    //récupère les coordonées x et y par rapport à la fenetre de navigation
    let x = event.pageX;
    let y = event.pageY;

    let canvas = document.getElementById("canvasElem");

    // récupère les coordonnées x et y par rapport au canvas
    // (supprime le décalage gauche et haut qui existe entre la fenetre et l'emplacement du canvas)
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;

    //
    x = Math.floor(x);
    y = Math.floor(y);

    // Appel la fonction pour le changement de couleur de la case avec en paramètre les coord x et y,
    // le type de clic sur la souris et le context 2d
    changeColor(x, y, event.button, context);
}

// Fonction de session de jeu
function gamePicross(ctx) {

    //Initialise le jeu, la grille de jeu et la grille d'indices
    gameInit();
    initGridArray();

    //Affiche la grille de jeu
    displayGrid(ctx);

    // Attend un clic de la souris pour executer la function dans la variable mouseEvent
    canvasElem.addEventListener("mousedown", mouseEvent);

}

function timer(){

    if(startTime === 0) {
        startTime = new Date();
    }
	end = new Date();
	diff = end - startTime;
	diff = new Date(diff);
	let sec = diff.getSeconds();
	let min = diff.getMinutes();
	let hr = diff.getHours()-1;
	if (min < 10){
		min = "0" + min;
	}
	if (sec < 10){
		sec = "0" + sec;
	}
	document.getElementById("timer").textContent = hr + ":" + min + ":" + sec;
	timerID = setTimeout(timer, 1000);
}

function timerStop(){
	
	clearTimeout(timerID);
}

function timerReset(){
    startTime = new Date();
	document.getElementById("timer").textContent = "0:00:00"
}

/////////////////////////////////////////////MAIN////////////////////////////////////////////////////////
// Au chargement de la page web ...
window.addEventListener('load', function () {

    // Empeche le clic droit d'afficher le menu contextuel
    preventDefaultClickRight();

    // Initialise le canvas et Récupère le contexte de dessin 2D canvas
    context = getContext();
    if (!context) {
        return;
    }

    // Si on clique sur le bouton titanic, on initialise le jeu pour titanic
    document.getElementById("titanic").addEventListener("click", function () {

        refreshGame(context)
        picrossColumns = titanicColumns;
        picrossRows = titanicRows;
        squareWidth = SQUARE_WIDTH_TITANIC;
        nbMaxClues = NB_MAX_CLUES_TITANIC;
        timerReset();

        gamePicross(context);
        timer();
        document.getElementById("verification").addEventListener("click", function () {
            displayWin();
        });

    });

    // Si on clique sur le bouton champignon, on initialise le jeu pour champignon
    document.getElementById("champignon").addEventListener("click", function () {

        refreshGame(context)
        picrossColumns = champignonColumns;
        picrossRows = champignonRows;
        squareWidth = SQUARE_WIDTH_CHAMPIGNON;
        nbMaxClues = NB_MAX_CLUES_CHAMPIGNON;
        timerReset();

        gamePicross(context);
        timer();
        document.getElementById("verification").addEventListener("click", function () {
            displayWin();
        });
    });

    // Si on clique sur le bouton cerf, on initialise le jeu pour cerf
    document.getElementById("cerf").addEventListener("click", function () {

        refreshGame(context)
        picrossColumns = cerfColumns;
        picrossRows = cerfRows;
        squareWidth = SQUARE_WIDTH_CERF;
        nbMaxClues = NB_MAX_CLUES_CERF;
        timerReset();

        gamePicross(context);
        timer();
        document.getElementById("verification").addEventListener("click", function () {
            displayWin();
        });

    });

}, false);